const express = require('express')
const app = express()
const port = 3333

const path = require('path')

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'pages', 'welcome.html'))
})

app.get('/blog', (req, res) => {
    res.sendFile(path.join(__dirname, 'pages', 'blog', 'blog.html'))
})

app.get('/stories', (req, res) => {
    res.sendFile(path.join(__dirname, 'pages', 'stories', 'stories.html'))
})

app.get('/contact', (req, res) => {
    res.sendFile(path.join(__dirname, 'pages', 'contact.html'))
})

app.listen(port, () => {
    console.log(`Server started @ ${port}`)
})
